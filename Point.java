public class Point {
    int xCoord;
    int yCoord;
    // Constructer
    public Point(int x , int y){
        xCoord=x;
        yCoord=y;
    }

    public double distance(Point point){
        return Math.sqrt((xCoord- point.xCoord )*( xCoord- point.xCoord)+(yCoord- point.yCoord)*(yCoord- point.yCoord));
    }

}
